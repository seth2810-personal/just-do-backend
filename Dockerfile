# build
FROM node:8-alpine AS build
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN npm install && \
    npm run build && \
    npm prune --production

# release
FROM node:8-alpine
LABEL authors "Roman Gafurov <gafurov.roma89@gmail.com>"
COPY --from=build /app/dist /app
COPY --from=build /app/node_modules /app/node_modules
COPY .docker/entrypoint.sh /
RUN chmod +x /entrypoint.sh
WORKDIR /app
ENTRYPOINT ["/entrypoint.sh"]
CMD ["node", "src/main.js"]
