import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const config: TypeOrmModuleOptions = {
  type: 'mongodb',
  useNewUrlParser: true,
  url: process.env.DATABASE_URL,
  entities: [
    'src/**/*.entity.{ts,js}',
  ],
};

export = config;
