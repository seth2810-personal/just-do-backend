import { Request, Response } from 'express';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiResponse, ApiOkResponse, ApiNotFoundResponse } from '@nestjs/swagger';
import {
  Controller, UseGuards, HttpStatus, Get, Post, Body, Req,
  ClassSerializerInterceptor, UseInterceptors, Param, Delete, NotFoundException, Res, Put,
} from '@nestjs/common';

import { AuthGuard } from '../auth/auth.guard';

import { Todo } from './todo.entity';
import { TodoService } from './todo.service';
import { CreateTodoDto } from './create-todo.dto';
import { UpdateTodoDto } from './update-todo.dto';

@Controller('todos')
@UseGuards(AuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class TodoController {
  constructor(
    private readonly todoService: TodoService,
  ) { }

  @Get()
  @ApiOkResponse({ description: 'Returns all user todos' })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'User is not authorized' })
  async findAll(@Req() req: Request): Promise<Todo[]> {
    const { userId } = req.session;

    return this.todoService.list(userId);
  }

  @Post()
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiCreatedResponse({ description: 'Todo succesfully created' })
  async create(@Body() createTodoDto: CreateTodoDto, @Req() req: Request): Promise<Todo> {
    const { userId } = req.session;

    return this.todoService.create(Object.assign({ userId }, createTodoDto));
  }

  @Get(':id')
  @ApiNotFoundResponse({ description: 'Todo not found' })
  @ApiOkResponse({ description: 'Returns todo' })
  async findOne(@Param('id') id: string): Promise<Todo> {
    const todo = await this.todoService.find(id);

    if (!todo) {
      throw new NotFoundException();
    }

    return todo;
  }

  @Put(':id')
  @ApiNotFoundResponse({ description: 'Todo not found' })
  @ApiOkResponse({ description: 'Todo succesfully updated' })
  async update(@Param('id') id: string, @Body() updateTodoDto: UpdateTodoDto): Promise<Todo> {
    const todo = await this.todoService.find(id);

    if (!todo) {
      throw new NotFoundException();
    }

    return this.todoService.update(id, updateTodoDto);
  }

  @Delete(':id')
  @ApiNotFoundResponse({ description: 'Todo not found' })
  @ApiResponse({ status: HttpStatus.NO_CONTENT, description: 'Todo succesfully deleted' })
  async remove(@Param('id') id: string, @Res() res: Response): Promise<void> {
    const todo = await this.todoService.find(id);

    if (!todo) {
      throw new NotFoundException();
    }

    await this.todoService.remove(todo);

    res.status(HttpStatus.NO_CONTENT).send();
  }
}
