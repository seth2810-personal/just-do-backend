import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsEnum, IsOptional, IsDateString } from 'class-validator';

import { PriorityType, AlarmType } from '../shared/constants';

export class UpdateTodoDto {
  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  title: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  text?: string;

  @IsEnum(PriorityType)
  @IsOptional()
  @ApiModelProperty({ enum: PriorityType, required: false })
  priority: PriorityType;

  @IsDateString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  dueDate: string;

  @IsEnum(AlarmType)
  @IsOptional()
  @ApiModelProperty({ enum: AlarmType, required: false })
  alarm: AlarmType;
}
