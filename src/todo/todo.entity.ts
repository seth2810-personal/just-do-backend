import { Exclude, Type } from 'class-transformer';
import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

import { PriorityType, AlarmType } from '../shared/constants';

@Entity('todos')
export class Todo {
  @ObjectIdColumn()
  @Type(() => String)
  id: ObjectID;

  @Column('string')
  title: string;

  @Column('string', { nullable: true })
  text?: string;

  @Column('string')
  priority: PriorityType;

  @Column('datetime')
  dueDate: string;

  @Column('string')
  alarm: AlarmType;

  @ObjectIdColumn()
  @Exclude({ toPlainOnly: true })
  userId: ObjectID;
}
