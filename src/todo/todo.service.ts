import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeepPartial, ObjectID, UpdateResult } from 'typeorm';

import { Todo } from './todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private readonly todoRepository: Repository<Todo>,
  ) { }

  async create(properties: DeepPartial<Todo>): Promise<Todo> {
    const todo = await this.todoRepository.create(properties);

    return this.todoRepository.save(todo);
  }

  async find(id: string): Promise<Todo> {
    return this.todoRepository.findOne(id);
  }

  async update(id: string, properties: DeepPartial<Todo>): Promise<Todo> {
    await this.todoRepository.update(id, properties);

    return this.todoRepository.findOne(id);
  }

  async remove(todo: Todo): Promise<void> {
    await this.todoRepository.remove(todo);
  }

  async list(userId: ObjectID): Promise<Todo[]> {
    return this.todoRepository.find({ userId });
  }
}
