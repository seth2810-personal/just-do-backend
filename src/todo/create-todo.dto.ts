import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsEnum, IsOptional, IsDateString } from 'class-validator';

import { PriorityType, AlarmType } from '../shared/constants';

export class CreateTodoDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  title: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  text?: string;

  @IsEnum(PriorityType)
  @ApiModelProperty({ enum: PriorityType })
  priority: PriorityType;

  @IsDateString()
  @ApiModelProperty()
  dueDate: string;

  @IsEnum(AlarmType)
  @ApiModelProperty({ enum: AlarmType })
  alarm: AlarmType;
}
