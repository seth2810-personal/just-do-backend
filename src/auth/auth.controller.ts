import { Response, Request } from 'express';
import { ApiConflictResponse, ApiBadRequestResponse, ApiCreatedResponse, ApiResponse } from '@nestjs/swagger';
import { Controller, Post, Body, Req, Res, ConflictException, UseGuards, HttpStatus, UnauthorizedException } from '@nestjs/common';

import { UserService } from '../user/user.service';
import { sessionCookieName } from '../shared/constants';

import { SingInDto } from './signin.dto';
import { SingUpDto } from './signup.dto';
import { AuthGuard } from './auth.guard';

@Controller('/')
export class AuthController {
  constructor(
    private readonly userService: UserService,
  ) { }

  @Post('signup')
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiCreatedResponse({ description: 'User succesfully registered' })
  @ApiConflictResponse({ description: 'User with same email already registered' })
  async signup(@Body() singupDto: SingUpDto): Promise<void> {
    const user = await this.userService.findByEmail(singupDto.email);

    if (user) {
      throw new ConflictException();
    }

    const password = this.userService.encodePassword(singupDto.password);

    await this.userService.create({ email: singupDto.email, password });
  }

  @Post('sigin')
  @ApiResponse({ status: HttpStatus.NO_CONTENT, description: 'User succesfully logged in' })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'User is not authorized' })
  async sigin(@Body() singinDto: SingInDto, @Req() req: Request, @Res() res: Response): Promise<void> {
    const user = await this.userService.findByEmail(singinDto.email);
    const password = this.userService.encodePassword(singinDto.password);

    if (!user || user.password !== password) {
      throw new UnauthorizedException();
    }

    req.session.userId = user.id;
    res.status(HttpStatus.NO_CONTENT).send();
  }

  @Post('signout')
  @UseGuards(AuthGuard)
  @ApiResponse({ status: HttpStatus.NO_CONTENT, description: 'User succesfully logged out' })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'User is not authorized' })
  signout(@Req() req: Request, @Res() res: Response) {
    req.session.destroy(() => {
      res.clearCookie(sessionCookieName);
      res.status(HttpStatus.NO_CONTENT).send();
    });
  }
}
