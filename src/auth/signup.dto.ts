import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsEmail, IsNotEmpty, MinLength, Validate } from 'class-validator';

import { customValidator } from '../shared/validators/custom-validator';
import { IsEqualsToValidator } from '../shared/validators/isequalsto.validator';

export class SingUpDto {
  @IsEmail()
  @ApiModelProperty()
  readonly email: string;

  @IsString()
  @MinLength(8)
  @customValidator('twoUpperCaseChars', (value: string) => {
    return value.indexOf(' ') === -1;
  })({
    message: 'password should not contain spaces',
  })
  @customValidator('twoUpperCaseChars', (value: string) => {
    return value.indexOf('\n') === -1;
  })({
    message: 'password should not line breaks',
  })
  @customValidator('twoUpperCaseChars', (value: string) => {
    return /.*[A-Z].*[A-Z].*/.test(value);
  })({
    message: 'password should contain at least two uppercase chars',
  })
  @customValidator('atLeastOneSpecialChar', (value: string) => {
    return /[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#]+/.test(value);
  })({
    message: 'password should contain at least one special character',
  })
  @customValidator('atLeastOneDigit', (value: string) => {
    return /[0-9]+/.test(value);
  })({
    message: 'password should contain at least one digit',
  })
  @ApiModelProperty()
  readonly password: string;

  @IsString()
  @IsNotEmpty()
  @Validate(IsEqualsToValidator, ['password'])
  @ApiModelProperty()
  readonly passwordConfirm: string;
}
