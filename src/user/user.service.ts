import * as crypto from 'crypto';
import { Injectable } from '@nestjs/common';
import { Repository, DeepPartial } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) { }

  async create(properties: DeepPartial<User>): Promise<User> {
    const user = await this.usersRepository.create(properties);

    return this.usersRepository.save(user);
  }

  async findByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({ email });
  }

  encodePassword(password: string): string {
    return crypto.createHash('sha256').update(password).digest('hex');
  }
}
