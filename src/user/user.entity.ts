import { Exclude } from 'class-transformer';
import { Entity, Column, ObjectIdColumn, ObjectID, Index } from 'typeorm';

@Entity('users')
export class User {
  @ObjectIdColumn()
  @Exclude({ toPlainOnly: true })
  id: ObjectID;

  @Column('string')
  @Index({ unique: true })
  email: string;

  @Column('string')
  @Exclude({ toPlainOnly: true })
  password: string;
}
