import * as session from 'express-session';
import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { sessionCookieName } from './shared/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('JustDo')
    .setDescription('JustDo backend API description')
    .setVersion('1.0')
    .addTag('just-do')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  app.use(session({
    resave: false,
    name: sessionCookieName,
    secret: 'session secret',
    saveUninitialized: false,
  }));

  app.useGlobalPipes(new ValidationPipe({
    forbidNonWhitelisted: true,
    whitelist: true,
  }));

  await app.listen(process.env.PORT || 3000);
}

bootstrap();
