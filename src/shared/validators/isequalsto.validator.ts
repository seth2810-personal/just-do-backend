import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';

@ValidatorConstraint({ name: 'isEqualsTo' })
export class IsEqualsToValidator implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    const { constraints, object: obj } = args;
    const [ relatedPropertyName ] = constraints;

    const relatedPropertyValue = obj[relatedPropertyName];

    return value === relatedPropertyValue;
  }

  defaultMessage(args: ValidationArguments) {
    const [ relatedPropertyName ] = args.constraints;

    return `${args.property} must be equals with ${relatedPropertyName}`;
  }
}
