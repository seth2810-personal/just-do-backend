import { ValidationOptions, ValidationArguments, registerDecorator } from 'class-validator';

export const customValidator = (name, validate) => (
  (validationOptions?: ValidationOptions) => (
    (object: object, propertyName: string) => {
      registerDecorator({
        name,
        propertyName,
        target: object.constructor,
        options: validationOptions,
        validator: {
          validate,
        },
      });
    }
  )
);
