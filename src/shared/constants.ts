export const sessionCookieName = 'connect.sid';

export enum PriorityType {
  Urgent = 'urgently',
  Important = 'important',
  Normal = 'normal',
  Neutral = 'neutral',
}

export enum AlarmType {
  FiveMinutes = '5m',
  TenMinutes = '10m',
  HalfAnHour = '30m',
  OneHour = '1h',
  ThreeHours = '3h',
  OneDay = '1d',
  OneWeek = '1w',
}
